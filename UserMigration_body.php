<?php
/**
 * User Migration
 * User Migration Special Page
 *
 * @author		Foxlit
 * @copyright	(c) 2010 Foxlit
 * @license		GNU General Public License v2.0 or later
 * @package		User Migration
 *
 * Includes maintenance and updates by Alexia E. Smith, Curse Inc.
 *
**/

class ClaimExternalAccount extends SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		global $wgMigrationSource, $wgMigrationSecret, $wgMigrationGroup;

		$this->wgRequest			= $this->getRequest();
		$this->output				= $this->getOutput();
		$this->wgUser				= $this->getUser();
		$this->wgMigrationSource	= $wgMigrationSource;
		$this->wgMigrationSecret	= $wgMigrationSecret;
		$this->wgMigrationGroup		= $wgMigrationGroup;

		$this->DB = wfGetDB(DB_MASTER);

		parent::__construct('ClaimExternalAccount');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		if (!$this->checkConfig()) {
			$this->output->showErrorPage('migration_error', 'error_not_configured');
			return;
		}

		if ($this->wgUser->isLoggedIn()) {
			$this->output->showErrorPage('migration_error', 'error_migration_logged_in');
			return;
		}

		$this->setHeaders();

		$this->userMigrationForm();
	}

	/**
	 * User Migration Form
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function userMigrationForm() {
		$username = trim($this->wgRequest->getVal('umUsername'));
		$email = trim($this->wgRequest->getVal('umEmail'));

		$message = $this->userMigrationSubmit($username);

		$param = $this->wgRequest->getText('param');

		$this->output->setPagetitle(wfMessage('reclaim-title'));
	
		$formTitle = Title::newFromText('Special:ClaimExternalAccount');
		$formURL = $formTitle->getFullUrl();
		if ($message === true) {
			$hydraAuthUser = HydraAuthUser::getInstance($this->user);
			if ($hydraAuthUser->getPremergeName() !== false && $this->user->getName() != $hydraAuthUser->getPremergeName()) {
				$this->output->addHTML("<strong>Due to a naming conflict with an existing Curse.com account your login user name has been renamed to '<em>{$this->user->getName()}</em>'.  You must use that name to log in, but you will still retain your old name as a display name.  If you own the Curse.com account with the '<em>{hydraAuthUser->getPremergeName()</em>' user name you will be given a chance to link it up after <a href='/Special:UserLogin'>logging into your migrated account.</a>");
			} else {
				$this->output->addHTML("You may now <a href='/Special:UserLogin'>login</a> using your account name and new password.");
			}
		} else {
			$this->output->addWikiText(wfMessage('reclaim-help'));

			$this->output->addHTML('<div id="userloginForm" class="reclaimForm"><form action="'.$formURL.'" id="userlogin" method="post">
			<h2>'.wfMessage('reclaim-title').'</h2>
			<span class="error">'.$message.'</span>
			<table>
			<tr>
				<td class="mw-label"><label for="umUsername">'.wfMessage('username').'</label></td>
				<td class="mw-input"><input type="text" name="umUsername" value="'.htmlspecialchars($username, ENT_QUOTES).'" /></td>
			</tr>
			<tr>
				<td class="mw-label"><label for="umEmail">'.wfMessage('new_email').'</label></td>
				<td class="mw-input"><input type="text" name="umEmail" value="'.htmlspecialchars($email, ENT_QUOTES).'" /></td>
			</tr>
			<tr>
				<td class="mw-label"><label for="umPassword">'.wfMessage('Newpassword').'</label></td>
				<td class="mw-input"><input type="password" name="umPassword" /></td>
			</tr>
			<tr>
				<td></td>
				<td class="mw-submit"><input type="submit" value="'.wfMessage('reclaim-action').'" name="umSubmit" /></td>
			</tr>
			</table>
			</form></div>');
		}
	}

	/**
	 * User Migration Form
	 *
	 * @access	public
	 * @param	string	Old Username
	 * @return	string	Message to be displayed with the form.
	 */
	public function userMigrationSubmit($username) {
		if ($this->wgRequest->wasPosted()) {
			$email = trim($this->wgRequest->getVal('umEmail'));
			$newpass = trim($this->wgRequest->getVal('umPassword'));

			$this->user = User::newFromName($username);

			if ($this->user === false || !$this->user->getId()) {
				$message = wfMessage('reclaim-not-reserved');
			} elseif (!$this->user->isValidPassword($newpass)) {
				$message = wfMessage('reclaim-invalid-password');
			} elseif (empty(trim($email)) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$message = wfMessage('reclaim-invalid-email');
			} elseif (!in_array($this->wgMigrationGroup, $this->user->getGroups())) {
				$message = wfMessage('reclaim-account-in-use');
			} else {
				$token = $this->generateToken($username, $newpass);
				$revVerified = $this->verifyTokenRevision($username, $token);
				if ($revVerified === true) {
					try {
						if ($newpass !== null) {
							if (!$this->user->isValidPassword($newpass)) {
								global $wgMinimalPasswordLength;
								$valid = $this->user->getPasswordValidity($newpass);
								if (is_array($valid)) {
									$message = array_shift($valid);
									$params = $valid;
								} else {
									$message = $valid;
									$params = array($wgMinimalPasswordLength);
								}
								throw new PasswordError(wfMessage($message, $params)->parse());
							}
						}

						$this->user->setEmail($email);
						$this->user->setInternalPassword($newpass);
						$mPassword = $this->user->mPassword;

						$this->user->removeGroup($this->wgMigrationGroup);
						$this->user->saveSettings();

						$this->DB->update(
							'user',
							[
								'user_password' => $mPassword,
								'user_email' => $email
							],
							['user_id' => $this->user->getId()],
							__METHOD__
						);

						$this->user->invalidateCache();

						$this->logMigration($this->user);
						return true;
					} catch (PasswordError $pwe) {
						$this->error($pwe->getText(), true);
					}
				} else {
					$message = wfMessage('reclaim-missing-token', $token, $username, wfMessage('reclaim-no-edit-summary'))->parse();
					$this->sendEmail($this->user, $message);
				}
			}	
		} else {
			$message = wfMessage('reclaim-begin');
		}
		return $message;
	}

	/**
	 * Verify Token Revision - Calls out to the old wiki through the API to verify the token exists on the page.
	 *
	 * @access	private
	 * @param	string	Old Username
	 * @param	string	Generated Token from generateToken()
	 * @return	boolean	Token Matched
	 */
	private function verifyTokenRevision($username, $token) {
		$comment = null;

		$unurl = rawurlencode($username);

		$apiPage = $this->wgMigrationSource.'?format=json&formatversion=2&action=query&prop=revisions&titles=User:'.$unurl.'&rvuser='.$unurl.'&rvprop=comment&rvlimit=1&random='.mt_rand();
		$request = \MWHttpRequest::factory($apiPage, [], __METHOD__);
		$status = $request->execute();
		if ($status->isOK()) {
			$data = @json_decode($request->getContent(), true);
			if (isset($data['query']['pages'])) {
				$firstPage = current($data['query']['pages']);
				$revision = current($firstPage['revisions']);
				if (isset($revision['comment'])) {
					$comment = trim($revision['comment']);
				}
			}
		}

		if ($comment === $token) {
			return true;
		}
		return false;
	}

	/**
	 * Generate Token - Creates a static token based off the username and password.
	 *
	 * @access	private
	 * @param	string	Old Username
	 * @param	string	New Password
	 * @return	string	Base64 encoded token
	 */
	private function generateToken($username, $password) {
		$secret = pack('H*', $this->wgMigrationSecret);
		$token = hash_hmac('sha256', $username.':'.$password, $secret);
		return base64_encode(pack('H*', substr($token,0, 16)));				
	}

	/**
	 * Log Migration - Inserts a log entry for the migration.
	 *
	 * @access	private
	 * @param	object	User Object
	 * @return	void
	 */
	private function logMigration($user) {
		$log = new LogPage('newusers');
		$log->addEntry(
						'reclaim',
						$user->getUserPage(),
						'',
						array($user->getId()),
						$user
					);
	}

	/**
	 * Check Configuration - Makes sure this extension is setup correctly before allowing migrations to occur.
	 *
	 * @access	private
	 * @return	boolean True if all the settings are good, false otherwise.
	 */
	private function checkConfig() {
		if (is_string($this->wgMigrationSource) && $this->wgMigrationSource && is_string($this->wgMigrationSecret) && $this->wgMigrationSecret && is_string($this->wgMigrationGroup) && $this->wgMigrationGroup) {
			return true;
		}
		return false;
	}

	/**
	 * Send the Email
	 *
	 * @access	private
	 * @param	object	Mediawiki User Object
	 * @param	string	Reclaim Message
	 * @return	void
	 */
	private function sendEmail($user, $message) {
		global $wgSitename, $wgNoReplyAddress;

		$emailTo		= "{$user->mName} <{$user->mEmail}>";
		$emailSubject	= "Email from the administrators of {$wgSitename}.";

		$emailContent	= "In case you lost or browsed away from the migration instructions to claim your external wiki account, they are as follows.<br/><br/>".$message;

		$emailHeaders	= "MIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\nFrom: {$wgNoReplyAddress}\r\nReply-To: {$wgNoReplyAddress}\r\nX-Mailer: CLI/1.0";

		$success = mail($emailTo, $emailSubject, $emailContent, $emailHeaders, '-f'.$wgNoReplyAddress);
	}

	/**
	 * Show/Hide special page from SpecialPages special page.
	 *
	 * @access	public
	 * @return	boolean
	 */
	public function isListed() {
		return true;
	}

	/**
	 * Is this page restricted based on permissions?
	 *
	 * @access	public
	 * @return	boolean False
	 */
	public function isRestricted() {
		return false;
	}
}
?>