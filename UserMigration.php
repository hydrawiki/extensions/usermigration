<?php
/**
 * User Migration
 * User Migration Mediawiki Settings
 *
 * @author		Foxlit
 * @copyright	(c) 2010 Foxlit
 * @license		GNU General Public License v2.0 or later
 * @package		User Migration
 *
 * Special:ClaimExternalAccount allows people to reset their local password
 * by making an edit to their user page on a wiki they already have access to.
 * 
 * We generate a token for a (username, password) pair, and then check
 * whether this token appears in the latest revision to the user's page
 * done by the user; if it does, we reset the local password to the 
 * desired	new value.
 * 
 * $wgMigrationSource specifies the URL of the api.php file of the old wiki.
 * $wgMigrationSecret is a 64-character hex string used to protect passwords
 * $wgMigrationGroup is the name of the group of users who have not yet reclaimed
 * their accounts.
 *
 * Includes maintenance and updates by Alexia E. Smith, Curse Inc.
 *
**/

if ( function_exists( 'wfLoadExtension' ) ) {
	wfLoadExtension( 'UserMigration' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['UserMigration'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for UserMigration extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
 } else {
	die( 'This version of the UserMigration extension requires MediaWiki 1.25+' );
}
